package TwoStringTask;

import java.util.Comparator;

public class StringComparator implements Comparator<DoubleStringContainer> {

  @Override
  public int compare(DoubleStringContainer o1, DoubleStringContainer o2) {
    return o1.getSecond().compareTo(o2.getSecond());
  }
}
