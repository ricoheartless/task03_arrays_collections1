package TwoStringTask;

import java.util.ArrayList;
import java.util.List;

public final class ListGenerator {

  private ListGenerator() {
  }
  public static List<DoubleStringContainer> generate(){
    List<DoubleStringContainer> list = new ArrayList<>();
    list.add(new DoubleStringContainer("Ukraine", "Kyiv"));
    list.add(new DoubleStringContainer("Norway", "Oslo"));
    list.add(new DoubleStringContainer("Cameroon","Yaounde"));
    list.add(new DoubleStringContainer("Canada","Ottawa"));
    list.add(new DoubleStringContainer("Japan","Tokyo"));
    list.add(new DoubleStringContainer("Poland","Warsaw"));
    list.add(new DoubleStringContainer("Belgium","Brussels"));
    return list;
  }
}
