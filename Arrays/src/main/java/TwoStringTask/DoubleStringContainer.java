package TwoStringTask;

public class DoubleStringContainer implements Comparable<DoubleStringContainer>{
  private String str1;
  private String str2;
  DoubleStringContainer(String first, String second){
    this.str1 = first;
    this.str2 = second;
  }
  @Override
  public int compareTo(DoubleStringContainer o) {
    return str1.compareTo(o.getFirst());
  }

  public String getFirst() {
    return str1;
  }

  public String getSecond() {
    return str2;
  }
}
