package TwoStringTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    DoubleStringContainer Japan = new DoubleStringContainer("Japan","Tokyo");
    List<DoubleStringContainer> list = new ArrayList<>(ListGenerator.generate());
    Collections.sort(list);
    System.out.println("Sorted by first only: ");
    for (DoubleStringContainer strings:list) {
      System.out.println(strings.getFirst() +" : "+strings.getSecond());
    }
    list.sort(new StringComparator());
    System.out.println("Sorted by second only: ");
    for (DoubleStringContainer strings:list) {
      System.out.println(strings.getFirst() +" : "+strings.getSecond());
    }
    System.out.println("Binary search using comparator: ");
    System.out.println(Collections.binarySearch(list,Japan,new StringComparator()));

  }

}
