package DequeTask;

public class DequeTest {

  public static void main(String[] args) {
    MyDeque <Integer> myDeque = new MyDeque<>();
    myDeque.addFirst(1);
    myDeque.addFirst(2);
    myDeque.addLast(9);
    myDeque.addLast(10);
    myDeque.removeFirst();
    myDeque.removeLast();
    System.out.println("Size: "+myDeque.size());
    System.out.println("First: "+myDeque.getFirst());
    System.out.println("Last: "+myDeque.getLast());
  }

}
