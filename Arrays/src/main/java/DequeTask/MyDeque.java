package DequeTask;


import java.lang.annotation.Target;

public class MyDeque<T> {

  private class Node<T> {

    private T element;
    private Node<T> nextElement;
    private Node<T> prevElement;

    Node(T element, Node nextElement, Node prevElement) {
      this.element = element;
      this.nextElement = nextElement;
      this.prevElement = prevElement;
    }

    Node(T element) {
      this.element = element;
      this.prevElement = null;
      this.nextElement = null;
    }

    Node() {
    }

    public T getElement() {
      return element;
    }

    public void setElement(T element) {
      this.element = element;
    }

    public Node<T> getNextElement() {
      return nextElement;
    }

    public void setNextElement(Node<T> nextElement) {
      this.nextElement = nextElement;
    }

    public Node<T> getPrevElement() {
      return prevElement;
    }

    public void setPrevElement(Node<T> prevElement) {
      this.prevElement = prevElement;
    }
  }

  Node<T> head;
  Node<T> tail;
  int size;

  MyDeque() {
    head = null;
    tail = null;
    size = 0;
  }

  public void addFirst(T object) {
    Node<T> newHead = new Node();
    newHead.element = object;
    if (head != null) {
      newHead.nextElement = head;
      head.prevElement = newHead;
    }
    head = newHead;
    if (tail == null) {
      tail = head;
    }
    size++;
  }

  public void addLast(T object) {
    Node newTail = new Node();
    newTail.element = object;

    if (tail != null) {
      newTail.prevElement = tail;
      tail.nextElement = newTail;
    }
    tail = newTail;
    if (head == null) {
      head = tail;
    }
    size++;
  }

  public void removeFirst() {
    Node oldHead = head;
    head = head.nextElement;

    if (head == null) {
      tail = null;
    } else {
      head.prevElement = null;
    }
    size--;
  }

  public void removeLast() {
    Node oldTail = tail;
    tail = oldTail.prevElement;

    if (tail == null)
      head = null;
    else
      tail.nextElement = null;

    size--;
  }

  public T getFirst() {
    if (head.getElement() != null) {
      return head.getElement();
    }
    return null;
  }

  public T getLast() {
    if (tail.getElement() != null) {
      return tail.getElement();
    }
    return null;
  }

  public int size() {
    return size;
  }
}
