package Game;

public abstract class RoomInsider {
  private int power;

  public abstract int getPower();
}
