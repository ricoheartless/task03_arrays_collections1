package StringContainerTask;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class StringContainer {

  private String[] array;
  int head = 0;

  StringContainer(int num) {
    array = new String[num];
  }

  public void add(String string) {
    if (head < array.length) {
      array[head] = string;
      head++;
    } else {
      array = Arrays.copyOf(array,array.length+1);
      array[head] = string;
      head++;
    }
  }

  public String get(int num) {
    if (num < array.length) {
      return array[num];
    } else {
      throw new NoSuchElementException();
    }
  }
  public int length(){
    return array.length;
  }
}
