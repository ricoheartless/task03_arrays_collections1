package StringContainerTask;

import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    long startTime = System.currentTimeMillis();
    StringContainer stringContainer = new StringContainer(2);
    stringContainer.add("First");
    stringContainer.add("Second");
    stringContainer.add("Third");
    for (int i = 0; i < stringContainer.length(); i++) {
      System.out.println(stringContainer.get(i));
    }
    long endTime = System.currentTimeMillis();
    System.out.println(
        "Time of String container performance: " + (endTime - startTime) + " milli seconds");
    startTime = System.currentTimeMillis();
    List<String> list = new ArrayList();
    list.add("First");
    list.add("Second");
    list.add("Third");
    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }
    endTime = System.currentTimeMillis();
    System.out.println(
        "Time of String list performance: " + (endTime - startTime) + " milli seconds");
  }
}
