package ArraysTasks.A_task;

import java.util.HashSet;
import java.util.Set;

public final class ArrayManager {

  public final static int[] getCrossValues(int[] arr1, int arr2[]) {
    Set<Integer> set = new HashSet();
    for (int i = 0; i < arr1.length; i++) {
      for (int j = 0; j < arr2.length; j++) {
        if (arr1[i] == arr2[j]) {
          set.add(arr1[i]);
        }
      }
    }
    return set.stream().mapToInt(i -> i).toArray();
  }

  public final static int[] getUniqueValues(int[] arr1, int arr2[]) {
    Set<Integer> set = new HashSet();
    int[] crossValues = getCrossValues(arr1, arr2);
    findUnique(arr1, set, crossValues);
    findUnique(arr2, set, crossValues);
    return set.stream().mapToInt(i -> i).toArray();
  }

  private static void findUnique(int[] arr2, Set<Integer> set, int[] crossValues) {
    for (int i = 0; i < arr2.length; i++) {
      set.add(arr2[i]);
      for (int j = 0; j < crossValues.length; j++) {
        if (arr2[i] == crossValues[j]) {
          set.remove(arr2[i]);
        }
      }
    }
  }

}
