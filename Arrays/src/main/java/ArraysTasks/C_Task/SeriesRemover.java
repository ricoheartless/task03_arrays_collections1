package ArraysTasks.C_Task;

import java.util.HashSet;
import java.util.Set;

public final class SeriesRemover {

  SeriesRemover() {
  }
  public static final int[] removeSeries(int[] arr){
    Set<Integer> set = new HashSet();
    for (int i = 0; i < arr.length; i++) {
      set.add(arr[i]);
    }
    return set.stream().mapToInt(i -> i).toArray();
  }
}