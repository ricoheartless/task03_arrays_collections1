package ArraysTasks;

import ArraysTasks.A_task.ArrayManager;
import ArraysTasks.B_task.DuplicateRemover;
import ArraysTasks.C_Task.SeriesRemover;

public class Main {

  public static void main(String[] args) {
    int[] arr1 = {1,2,3,4,5,6};
    int[] arr2 = {10,11,4,5,6,9};
    System.out.println("Unmodified arrays: ");
    ArrayHelper.showArray(arr1);
    ArrayHelper.showArray(arr2);
    int[] cross = ArrayManager.getCrossValues(arr1,arr2);
    System.out.println("Common values in new array: ");
    ArrayHelper.showArray(cross);
    int[] unique = ArrayManager.getUniqueValues(arr1,arr2);
    System.out.println("Uncommon values in new array: ");
    ArrayHelper.showArray(unique);
    int[] duplicates = {1,4,3,3,4,4,2,4};
    System.out.println("Unmodified array: ");
    ArrayHelper.showArray(duplicates);
    int [] res = DuplicateRemover.removeMoreThanTwice(duplicates);
    System.out.println("Array without more than two duplicates: ");
    ArrayHelper.showArray(res);
    int [] series = {1,2,2,2,2,3,3,3,4,4,4,5};
    System.out.println("Array without modifications: ");
    ArrayHelper.showArray(series);
    res = SeriesRemover.removeSeries(series);
    System.out.println("Array with removed series: ");
    ArrayHelper.showArray(res);
  }
}
