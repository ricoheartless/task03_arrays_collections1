package ArraysTasks.B_task;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class DuplicateRemover {
   DuplicateRemover(){}
   public final static int[] removeMoreThanTwice(int[] arr){
     List<Integer> list = new ArrayList();
     Set<Integer> toRemove = new HashSet();
     int count = 0;
     for (int i = 0; i < arr.length; i++) {
       list.add(arr[i]);
       for (int j = 0; j < arr.length; j++) {
         if(arr[i] == arr[j]){
           count++;
         }
       }
       if(count > 2){
         toRemove.add(arr[i]);
       }
       count = 0;
     }
     list.removeAll(toRemove);
     return list.stream().mapToInt(i -> i).toArray();
   }
}
